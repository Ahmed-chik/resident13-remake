/client/verb/ban(var/target_ckey as text,var/ban_duration_minutes = -1 as num, var/ban_reason = "No reason given." as message)

	set name = "Ban ckey"
	set category = "Admin"

	ban_duration_minutes = text2num(ban_duration_minutes)

	if(!ban_duration_minutes)
		return FALSE

	if(!SSban)
		return FALSE

	if(!target_ckey)
		return FALSE

	SSban.add_ckey_ban(target_ckey,ckey,ban_reason,ban_duration_minutes == -1 ? -1 : world.realtime + ban_duration_minutes*60)
	return TRUE

/client/verb/jump_to_player()
	set name = "Jump to Player"
	set category = "Admin"

	sortTim(all_mobs_with_clients,/proc/cmp_path_asc)

	var/mob/choice = input("Who would you like to jump to?","Jump to Mob") as null|mob in all_mobs_with_clients
	if(!choice)
		to_chat(span("warning","Invalid mob."))
		return FALSE

	var/turf/T = get_turf(choice)
	if(!T)
		to_chat(span("warning","Invalid turf."))
		return FALSE

	mob.force_move(T)

	to_chat(span("notice","You jumped to [choice]'s location."))
	//log_admin("[src] jumped to [choice]'s (mob) location.")


/client/verb/jump_to_area()
	set name = "Jump to Area"
	set category = "Admin"

	var/list/coverted_choice = list()
	for(var/k in all_areas)
		var/area/A = all_areas[k]
		coverted_choice["[A]"] = A

	var/choice = input("What area would you like to jump to?","Jump to Area") as null|anything in coverted_choice
	if(!choice)
		to_chat(span("warning","Invalid area."))
		return FALSE

	var/area/A = coverted_choice[choice]

	var/turf/T = locate(A.average_x,A.average_y,A.z)
	if(!T)
		to_chat(span("warning","Invalid turf."))
		return FALSE

	mob.force_move(T)

	to_chat(span("notice","You jumped to [A]'s location."))
	//log_admin("[src] jumped to [A]'s (area) location.")