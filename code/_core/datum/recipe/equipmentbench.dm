/recipe/equipment/
	name = "workstation recipe"
	desc = "A workstation recipe"
	desc_extended = "Workstation recipe information."


	required_item_grid = list()
	required_items = list()

	recipe_type = "equipmentbench"

/recipe/equipment/samopal_mag

	name = "Магазин"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "sam_mag",
		"b3" = null,

		"c2" = "recharger"
	)

	product = /obj/item/magazine/samopal

/recipe/equipment/b_mag

	name = "Магазин"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "b_mag",
		"b3" = null,

		"c2" = "recharger"
	)

	product = /obj/item/magazine/rifle_556

/recipe/equipment/m_mag

	name = "Магазин"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "m_mag",
		"b3" = null,

		"c2" = "recharger"
	)

	product = /obj/item/magazine/rifle_556_1

/recipe/equipment/lmg_mag

	name = "Магазин"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "lmg_mag",
		"b3" = null,

		"c2" = "recharger"
	)

	product = /obj/item/magazine/lmg_762

/recipe/equipment/uzi_mag

	name = "Магазин"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "uzi_mag",
		"b3" = null,

		"c2" = "recharger"
	)

	product = /obj/item/magazine/destro_smg

/recipe/equipment/pistol_mag

	name = "Магазин"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "colt_mag",
		"b3" = null,

		"c2" = "recharger"
	)

	product = /obj/item/magazine/pistol_45

/recipe/equipment/sniper

	name = "Самопальная снайперская винтовка"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "samopal",
		"b3" = "mod_scope",

		"c2" = "mod"
	)

	product = /obj/item/weapon/ranged/bullet/magazine/rifle/samopal

/recipe/equipment/rev1

	name = "Улучшенный револьвер"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "rev_smoll",
		"b3" = null,

		"c2" = "mod"
	)

	product = /obj/item/weapon/ranged/bullet/revolver/commander

/recipe/equipment/rev2

	name = "Улучшенный револьвер"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "rev_stand",
		"b3" = null,

		"c2" = "mod"
	)

	product = /obj/item/weapon/ranged/bullet/revolver/commander/big

/recipe/equipment/rev_jhonny

	name = "Револьвер с прицелом"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "big_rev_jhonny",
		"b3" = "mod_scope",

		"c2" = "mod"
	)

	product = /obj/item/weapon/ranged/bullet/revolver/big_game/scope

/recipe/equipment/fbarrel

	name = "Четырёхствольный дробовик"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "dbarrel",
		"b3" = null,

		"c2" = "mod"
	)

	product = /obj/item/weapon/ranged/bullet/revolver/dbarrel/four

/recipe/equipment/sbarrel

	name = "Шестиствольный дробовик"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "fbarrel",
		"b3" = null,

		"c2" = "mod"
	)

	product = /obj/item/weapon/ranged/bullet/revolver/dbarrel/six

/recipe/equipment/axe_electric

	name = "Топор"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = "axe",
		"b2" = null,
		"b3" = "mod",

		"c2" = "blueprint"
	)

	product = /obj/item/weapon/melee/sword/resident/axe/electric

/recipe/equipment/spear

	name = "Копьё с заострённым концом"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = "shard",
		"b2" = null,
		"b3" = "dutch",

		"c2" = "spearstels"
	)

	product = /obj/item/weapon/melee/spear

/recipe/equipment/combat_shottie

	name = "Тактический дробовик"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "com_bat",
		"b3" = null,

		"c2" = "mod"
	)

	product = /obj/item/weapon/ranged/bullet/pump/shotgun/combat/mod_2

/recipe/equipment/bat_wires

	name = "Бита с проволокой"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = "wood",
		"b2" = "wire",
		"b3" = "wood",

		"c2" = "blueprint"
	)

	product = /obj/item/weapon/melee/resident/bat/up2

/recipe/equipment/bat_nails

	name = "Бита с гвоздями"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = "wood",
		"b2" = "mod",
		"b3" = "wood",

		"c2" = "blueprint"
	)

	product = /obj/item/weapon/melee/resident/bat/up1

/recipe/equipment/bat_saw

	name = "Бита с пилой"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = "wood",
		"b2" = "scrap",
		"b3" = "wood",

		"c2" = "blueprint"
	)

	product = /obj/item/weapon/melee/resident/bat/up3

/recipe/equipment/bat_upgrade1

	name = "Бита с гвоздями"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "bat",
		"b3" = null,

		"c2" = "mod"
	)

	product = /obj/item/weapon/melee/resident/bat/up1

/recipe/equipment/bat_upgrade2

	name = "Бита с проволокой"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = null,
		"b2" = "bat",
		"b3" = null,

		"c2" = "wire"
	)

	product = /obj/item/weapon/melee/resident/bat/up2

/recipe/equipment/bat_upgrade3

	name = "Бита с пилой"

	required_item_grid = list( //The second value is the craft_id of the item.
		"b1" = "scrap",
		"b2" = "bat",
		"b3" = "scrap",

		"c2" = "blueprint"
	)

	product = /obj/item/weapon/melee/resident/bat/up3