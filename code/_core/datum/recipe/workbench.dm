/recipe/workbench/
	name = "workbench recipe"
	desc = "A workbench recipe"
	desc_extended = "Workbench recipe information."


	required_item_grid = list()
	required_items = list()

	recipe_type = "workbench"

/recipe/workbench/sniper

	name = "Самопальная снайперская винтовка"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = "scrap",
		"a2" = "scrap",
		"a3" = null,

		"b1" = "samopal",
		"b2" = null,
		"b3" = null,

		"c1" = null,
		"c2" = null,
		"c3" = "dutch"
	)

	product = /obj/item/weapon/ranged/bullet/magazine/rifle/samopal

/recipe/workbench/rod

	name = "Стержень"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = "scrap",
		"b2" = "scrap",
		"b3" = null,

		"c1" = null,
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/material/rod/steel


/recipe/workbench/spear_weak

	name = "Копьё"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = "dutch",
		"b2" = "rod",
		"b3" = null,

		"c1" = "rod",
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/weapon/melee/resident/spear_stles

/*/recipe/workbench/spear

	name = "Копьё с заострённым концом"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = "shard",

		"b1" = null,
		"b2" = "dutch",
		"b3" = null,

		"c1" = "spearstels",
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/weapon/melee/spear*/

/recipe/workbench/wood_barricade

	name = "Деревянная баррикада"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = "wood",
		"a2" = "wood",
		"a3" = "wood",

		"b1" = "wood",
		"b2" = null,
		"b3" = "wood",

		"c1" = "wood",
		"c2" = "wood",
		"c3" = "wood"
	)

	product = /obj/item/deployable/barricade_wood

/recipe/workbench/wood_handle

	name = "Деревянная ручка"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = "wood",
		"b2" = null,
		"b3" = null,

		"c1" = null,
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/resident/crafting/ruchka

/recipe/workbench/metal_blade

	name = "Лезвие"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = "scrap",
		"b2" = null,
		"b3" = null,

		"c1" = null,
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/resident/crafting/blade

/recipe/workbench/metal_knife

	name = "Самопальный нож"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = "dutch",
		"b2" = "blade",
		"b3" = null,

		"c1" = "ruchka",
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/weapon/melee/sword/resident/knife

/recipe/workbench/big_handle

	name = "Деревянная ручка"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = null,
		"b2" = "wood",
		"b3" = null,

		"c1" = "wood",
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/resident/crafting/ruchka_big

/recipe/workbench/torch

	name = "Факел"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = null,
		"b2" = "cloth",
		"b3" = null,

		"c1" = "ruchka_big",
		"c2" = null,
		"c3" = "alcohol"
	)

	product = /obj/item/weapon/melee/torch

/recipe/workbench/cloth

	name = "Тряпки"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = "clothing",
		"b2" = null,
		"b3" = null,

		"c1" = null,
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/resident/crafting/cloth

/recipe/workbench/bandage

	name = "Бинт"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = "cloth",
		"b2" = null,
		"b3" = null,

		"c1" = null,
		"c2" = null,
		"c3" = "alcohol"
	)

	product = /obj/item/container/medicine/bandage

/recipe/workbench/cloth2

	name = "Тряпки"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = null,
		"a3" = null,

		"b1" = "skin",
		"b2" = null,
		"b3" = null,

		"c1" = null,
		"c2" = null,
		"c3" = null
	)

	product = /obj/item/resident/crafting/cloth

/recipe/workbench/arrow

	name = "Стрела"

	required_item_grid = list( //The second value is the craft_id of the item.
		"a1" = null,
		"a2" = "blade",
		"a3" = null,

		"b1" = null,
		"b2" = "rod",
		"b3" = null,

		"c1" = null,
		"c2" = "rod",
		"c3" = null
	)

	product = /obj/item/bullet_cartridge/bolt