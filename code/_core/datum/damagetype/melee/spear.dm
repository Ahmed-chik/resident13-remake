/damagetype/melee/spear/
	name = "spear"

	attack_damage_base = list(
		PIERCE = 40,
		BLADE = 30
	)

	attack_damage_penetration = list(
		PIERCE = 70,
		BLADE = 60
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 30,
		ATTRIBUTE_DEXTERITY = 20
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = PIERCE,
		ATTRIBUTE_DEXTERITY = BLADE
	)

	skill_stats = list(
		SKILL_MELEE = 30
	)

	skill_damage = list(
		SKILL_MELEE = PIERCE
	)

/damagetype/melee/spear/thrown

	name = "thrown spear"

	attack_damage_base = list(
		PIERCE = 90,
		BLADE = 65
	)

	attack_damage_penetration = list(
		PIERCE = 140,
		BLADE = 140
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 40,
		ATTRIBUTE_DEXTERITY = 20
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = PIERCE,
		ATTRIBUTE_DEXTERITY = PIERCE
	)

	skill_stats = list(
		SKILL_MELEE = 10,
		SKILL_RANGED = 20
	)

	skill_damage = list(
		SKILL_MELEE = PIERCE,
		SKILL_RANGED = PIERCE
	)

/damagetype/melee/spear_weak/
	name = "spear"

	attack_damage_base = list(
		PIERCE = 20,
		BLADE = 10
	)

	attack_damage_penetration = list(
		PIERCE = 15,
		BLADE = 10
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 30,
		ATTRIBUTE_DEXTERITY = 20
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = PIERCE,
		ATTRIBUTE_DEXTERITY = BLADE
	)

	skill_stats = list(
		SKILL_MELEE = 30
	)

	skill_damage = list(
		SKILL_MELEE = PIERCE
	)

/damagetype/melee/spear/thrown_weak

	name = "thrown spear"

	attack_damage_base = list(
		PIERCE = 25,
		BLADE = 15
	)

	attack_damage_penetration = list(
		PIERCE = 20,
		BLADE = 5
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 40,
		ATTRIBUTE_DEXTERITY = 20
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = PIERCE,
		ATTRIBUTE_DEXTERITY = PIERCE
	)

	skill_stats = list(
		SKILL_MELEE = 10,
		SKILL_RANGED = 20
	)

	skill_damage = list(
		SKILL_MELEE = PIERCE,
		SKILL_RANGED = PIERCE
	)