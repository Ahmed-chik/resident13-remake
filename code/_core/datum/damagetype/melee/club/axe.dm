/damagetype/melee/axe/
	name = "axe"

	attack_damage_base = list(
		PIERCE = 50,
		BLADE = 40
	)

	attack_damage_penetration = list(
		PIERCE = 80,
		BLADE = 70
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 40,
		ATTRIBUTE_DEXTERITY = 30
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = PIERCE,
		ATTRIBUTE_DEXTERITY = BLADE
	)

	skill_stats = list(
		SKILL_MELEE = 30
	)

	skill_damage = list(
		SKILL_MELEE = PIERCE
	)

/damagetype/melee/axe_electric
	name = "axe"

	attack_damage_base = list(
		PIERCE = 50,
		BLADE = 40,
		HEAT = 60
	)

	attack_damage_penetration = list(
		PIERCE = 80,
		BLADE = 70,
		HEAT = 50
	)

	attribute_stats = list(
		ATTRIBUTE_STRENGTH = 40,
		ATTRIBUTE_DEXTERITY = 30
	)

	attribute_damage = list(
		ATTRIBUTE_STRENGTH = PIERCE,
		ATTRIBUTE_DEXTERITY = BLADE
	)

	skill_stats = list(
		SKILL_MELEE = 30
	)

	skill_damage = list(
		SKILL_MELEE = PIERCE
	)