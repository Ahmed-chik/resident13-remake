/damagetype/ranged/magic/chaos
	name = "chaos ball"

	//The base attack damage of the weapon. It's a flat value, unaffected by any skills or attributes.
	attack_damage_base = list(
		BRUTE = 70
	)

	attribute_stats = list(
		ATTRIBUTE_INTELLIGENCE = 10,
		ATTRIBUTE_WILLPOWER = 10
	)

	attribute_damage = list(
		ATTRIBUTE_INTELLIGENCE = list(DARK)
	)

	skill_stats = list(
		SKILL_SORCERY = 10,
		SKILL_MAGIC = 10
	)

	skill_damage = list(
		SKILL_SORCERY = list(DARK)
	)