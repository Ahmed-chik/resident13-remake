/loadout/hop
	spawning_items = list(
		/obj/item/clothing/head/hat/hop,
		/obj/item/clothing/uniform/hop,
		/obj/item/clothing/glasses/sun,
		/obj/item/clothing/hands/gloves/colored,
		/obj/item/clothing/hands/gloves/colored/left,
		/obj/item/clothing/feet/shoes/brown,
		/obj/item/clothing/feet/shoes/brown/left
	)


/loadout/ce
	spawning_items = list(
		/obj/item/clothing/uniform/ce,
		/obj/item/clothing/hands/gloves/colored/insulated,
		/obj/item/clothing/hands/gloves/colored/insulated/left,
		/obj/item/clothing/feet/shoes/black_boots/left
	)


/loadout/hos
	spawning_items = list(
		/obj/item/clothing/head/hat/hos,
		/obj/item/clothing/uniform/res/destro_op,
		/obj/item/clothing/glasses/sun,
		/obj/item/clothing/overwear/armor/res/destrovest,
		/obj/item/clothing/overwear/coat/hos,
		/obj/item/clothing/hands/gloves/colored/combat,
		/obj/item/clothing/hands/gloves/colored/combat/left,
		/obj/item/clothing/feet/shoes/colored/black,
		/obj/item/clothing/feet/shoes/colored/black/left
	)