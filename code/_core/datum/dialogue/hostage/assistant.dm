/dialogue/hostage/assistant/


/dialogue/hostage/assistant/get_dialogue_options(var/mob/living/advanced/player/P,var/list/known_options)

	. = ..()

	var/mob/living/advanced/A = P.dialogue_target

	if(istype(A) && A.handcuffed)
		.["hello"] = list(
			"Эти ублюдки закрыли меня тут и надели эти грёбанные #1 мне на руки. Сними их с меня и давай сваливать.",
			"наручники",
			"*жди тут",
			"*следуй за мной"
		)
	else
		.["hello"] = list(
			pick("Как дела?","Как настроение?","Сейчас бы пива..."),
			"*жди тут",
			"*следуй за мной"
		)

	.["наручники"] = list(
		"Просто сними их уже!"
	)

	return .