/dialogue/npc/radio/

/dialogue/npc/radio/get_dialogue_options(var/mob/living/advanced/player/P,var/list/known_options)

	. = ..()

	if(known_options["Проигнорировать передачу"])
		.["hello"] = list(
			"*На частоте одни помехи*"
		)
	if(known_options["Попытаться наладить связь"])
		.["hello"] = list(
			"*На частоте одни помехи*"
		)
	else
		.["hello"] = list(
			"...северная Англия. Повторяю, говорит 15-ая ячейка, северная Англия. Нас кто-нибудь слышит?",
			"Слышим вас хорошо",
			"Проигнорировать передачу"
		)

	.["Слышим вас хорошо"] = list(
		"О боже...Серж, кажется мы на кого-то вышли! *шебуршание* С кем мы имеем честь говорить?",
		"Назвать своё настоящее имя",
		"Проигнорировать передачу"
	)

	.["Проигнорировать передачу"] = list(
		"*На частоте одни помехи*"
	)

	.["Назвать своё настоящее имя"] = list(
		"[P.real_name]? Вильям, пробей по базе. А вы оставайтесь на линии.",
		"Проигнорировать передачу",
		"Дождаться ответа"
	)

	.["Дождаться ответа"] = list(
		"Назовите ваше местоположение.",
		"Я не знаю",
		"Какая-то лаборатория"
	)

	.["Какая-то лаборатория"] = list(
		"Поезд? Железная дорога...Точно, старые служебные пути Дестро. Чёрт возьми, они смогут добраться к нам по прямой?",
		"Вы нам поможете",
		"Кто вы такие"
	)

	.["Я не знаю"] = list(
		"Тогда узнайте! Там должны быть какие-то опозновательные черты."
	)

	.["Кто вы такие"] = list(
		"Военные. Больше вам знать не нужно."
	)

	.["Вы нам поможете"] = list(
		"Отсюда? Нет. Но если вы сможете добраться до нас - вполне. Вам придётся прокатиться... \
		И я не знаю, сколько это в принципе займёт времени. \
		Магистраль, по которой идёт ваш монорельс - соединён с огромной сетью, построенной корпами ещё до всего этого пиздеца. \
		Если за время она не превратилась в кашу - то вы без труда сможете попасть по ней к нам. Наши координаты: X: 5987 Y: 3024. \
		Мы...бываем...остр...скоро...",
		"Попытаться наладить связь"
	)

	.["Попытаться наладить связь"] = list(
		"*На частоте одни помехи*"
	)