/obj/item/clothing/overwear/armor/res/destrovest
	name = "\improper D.E.S.T.R.O. LLC body armor"
	icon = 'icons/obj/item/clothing/suit/destro_op.dmi'
	desc = "Броня силовых ячеек Дестро."
	desc_extended = "Экземпляр комплекта брони одного из ЧВК, некогда связанного с D.E.S.T.R.O. Выглядит не очень, но какое-то количество пуль точно выдержит."

	protected_limbs = list(BODY_TORSO,BODY_GROIN,BODY_LEG_LEFT,BODY_LEG_RIGHT)

	rarity = RARITY_UNCOMMON
	slowdown_mul_worn = 0.9

	defense_rating = list(
		BLADE = 30,
		BLUNT = 70,
		PIERCE = 60,
		LASER = 20,
		MAGIC = -100
	)

	size = SIZE_3
	weight = WEIGHT_3

	value = 150