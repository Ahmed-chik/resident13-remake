/obj/item/clothing/head/res/moth
	name = "really big moth"
	icon = 'icons/obj/item/clothing/hats/moth.dmi'
	desc = "Отличный питомец."
	desc_extended = "Огромная по размерам моль, явно не собирающаяся высосать ваш мозг."
	rarity = RARITY_RARE

	defense_rating = list(
		MAGIC = INFINITY,
		DARK = INFINITY,
		HOLY = INFINITY
	)

	size = SIZE_2
	weight = WEIGHT_1

	value = 100