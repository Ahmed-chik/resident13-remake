/obj/item/clothing/head/helmet/security/old
	name = "Старый полицейский шлем"
	icon = 'icons/obj/item/clothing/hats/security.dmi'
	desc = "Отлично защищает от камней...Наверно"
	desc_extended = "Старый металлический шлем с забралом. Такой когда-то использовался полицейскими и некоторыми опер-группами"

	defense_rating = list(
		BLADE = 25,
		BLUNT = 30,
		PIERCE = 70,
		MAGIC = -25,
		BOMB = 50
	)

	size = SIZE_3
	weight = WEIGHT_2

	value = 20