/obj/item/bullet_cartridge/nail/
	name = "\improper ������"
	desc = "������������ ������."
	icon = 'icons/obj/item/bullet/nails.dmi'

	item_count_max = 4
	item_count_max_icon = 4

	bullet_diameter = 4.6
	bullet_length = 30

	projectile = /obj/projectile/bullet/firearm/nail
	damage_type_bullet = /damagetype/ranged/bullet/nails


	projectile_speed = TILE_SIZE * 0.75

	misfire_chance = 5

	size = 0.1
	weight = 0.5
	value = 10