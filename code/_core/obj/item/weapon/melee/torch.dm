/obj/item/weapon/melee/torch //Also flashlights.
	name = "torch"
	desc = "A staple of cavemen and revolutionaries."
	desc_extended = "Emits light."
	icon = 'icons/obj/item/weapons/melee/clubs/torch.dmi'

	var/enabled = FALSE
	var/charge = 1
	var/max_charge = 1
	var/torch_time = 6000

	damage_type = /damagetype/melee/club/torch/
	var/damage_type_on = /damagetype/melee/club/torch/on

	override_icon_state = TRUE
	override_icon_state_held = TRUE

	desired_light_range = VIEW_RANGE*0.16
	desired_light_power = 0.20
	desired_light_color = "#FFD175"

	attack_delay = 5
	attack_delay_max = 10

	value = 10

/obj/item/weapon/melee/torch/proc/charge_left(caller)// TODO: Переделать эту ебанутую систему
	if(enabled == TRUE)
		if(charge >= 0)
			charge -= 10
			if(istype(src, /obj/item/weapon/melee/torch/flashlight/meh_gen/type_2))// || object.name == "FNM Type 2") - на крайний случай
				if(charge >= 900)
					desired_light_range = VIEW_RANGE*0.6*1.1
					desired_light_power = 0.6*1
				else if(charge > 800)
					desired_light_range = VIEW_RANGE*0.6*1
					desired_light_power = 0.6*0.9
				else if(charge > 700)
					desired_light_range = VIEW_RANGE*0.6*0.9
					desired_light_power = 0.6*0.8
				else if(charge > 600)
					desired_light_range = VIEW_RANGE*0.6*0.8
					desired_light_power = 0.6*0.7
				else if(charge > 500)
					desired_light_range = VIEW_RANGE*0.6*0.7
					desired_light_power = 0.6*0.6
				else if(charge > 400)
					desired_light_range = VIEW_RANGE*0.6*0.6
					desired_light_power = 0.6*0.5
				else if(charge > 300)
					desired_light_range = VIEW_RANGE*0.6*0.5
					desired_light_power = 0.6*0.4
				else if(charge > 200)
					desired_light_range = VIEW_RANGE*0.6*0.4
					desired_light_power = 0.6*0.3
				else if(charge > 100)
					desired_light_range = VIEW_RANGE*0.6*0.3
					desired_light_power = 0.6*0.2
				else if(charge > 0)
					desired_light_range = VIEW_RANGE*0.6*0.2
					desired_light_power = 0.6*0.1
				else if(charge <= 0)
					desired_light_range = 1
					desired_light_power = 1
					enabled = FALSE
					set_light(FALSE)
			for(var/mob/living/advanced/A in range(0.1))
				if(get_dist(A, src) < 1)
					if(charge == max_charge*0.5)
						A.to_chat("Фонарик слабеет")
					else if(charge == max_charge*0.3)
						A.to_chat("Фонарик слабеет ещё сильнее")
					else if(charge == max_charge*0.1)
						A.to_chat("Фонарик сейчас сядет")
			if(enabled == TRUE)
				set_light(desired_light_range, desired_light_power, desired_light_color,desired_light_angle)
			spawn(5)
				charge_left(caller)
		else if(charge <= 0)
			enabled = FALSE
			update_icon()
			charge = 0 //Дабы избавиться от возможных отрицательных чисел
			update_sprite()
	else return

/obj/item/weapon/melee/torch/clicked_on_by_object(var/mob/caller,object,location,control,params)
	INTERACT_CHECK
	if(istype(src, /obj/item/weapon/melee/torch/flashlight) || istype(src, /obj/item/weapon/melee/torch/lantern))
		if(istype(src, /obj/item/weapon/melee/torch/lantern) && istype(object, /obj/item/fuel/lant))
			var/obj/item/weapon/melee/torch/lantern/O = src
			var/obj/item/fuel/lant/F = object
			O.charge = O.max_charge
			qdel(F)
		else if(istype(src, /obj/item/weapon/melee/torch/flashlight) && istype(object, /obj/item/fuel/flash))
			var/obj/item/weapon/melee/torch/flashlight/O = src
			var/obj/item/fuel/flash/F = object
			O.charge = O.max_charge
			qdel(F)
	if(istype(src, /obj/item/weapon/melee/torch/flashlight/meh_gen/type_2))
		if(charge < max_charge)
			charge += 20
		if(enabled != TRUE)
			enabled = TRUE
			charge_left(src, caller)
	else if(istype(src, /obj/item/weapon/melee/torch/flashlight) || istype(src, /obj/item/weapon/melee/torch/lantern))
		if(istype(src, /obj/item/weapon/melee/torch/flashlight/meh_gen/type_2))
			return
		if(istype(src, /obj/item/weapon/melee/torch/flashlight/meh_gen/type_1) && istype(object, /obj/item/fuel/meh_gen))
			if(charge < max_charge)
				charge += 20
			create_alert(11, caller, caller)
			//charge_left(caller) //На случай, если надо ограничить подзарядку фонаря при включенном состоянии
			//todo: сделать оповестительный звук о перезарядке фонарика(ибо влипнут все от этого)
		else if(enabled == TRUE)
			enabled = !enabled
		else if(enabled == FALSE && (charge > 0))
			charge -= 100
			enabled = TRUE
			charge_left(src, caller)
	else
		if(enabled == TRUE)
			caller.to_chat("Факел тушится, вряд ли выйдет зажечь его ещё раз")
			enabled = FALSE
			charge = 0
			update_sprite()
			qdel(src)
		else if(enabled == FALSE)
			if(charge == 1)
				enabled = TRUE
				spawn(torch_time)
					enabled = FALSE
					charge = 0
					update_sprite()
					caller.to_chat("Факел сгорел")
					qdel(src)
			if(charge == 0)
				return
	update_sprite()
	return TRUE
	..()

/obj/item/weapon/melee/torch/update_icon()
	if(enabled)
		damage_type = damage_type_on
		icon_state = "[initial(icon_state)]_on"
		icon_state_held_left = "[initial(icon_state_held_left)]_on"
		icon_state_held_right = "[initial(icon_state_held_right)]_on"
		set_light(desired_light_range, desired_light_power, desired_light_color,desired_light_angle)
	else
		damage_type = initial(damage_type)
		icon_state = initial(icon_state)
		icon_state_held_left = initial(icon_state_held_left)
		icon_state_held_right = initial(icon_state_held_right)
		set_light(FALSE)

	update_held_icon()

	..()

/obj/item/weapon/melee/torch/lantern
	name = "lantern"
	desc = "But what type of lantern? Eh, who cares."
	desc_extended = "Emits a lot of light."
	icon = 'icons/obj/item/weapons/melee/clubs/lantern.dmi'

	max_charge = 3000
	charge = 3000

	damage_type = /damagetype/item/medium
	damage_type_on = /damagetype/item/medium

	override_icon_state = TRUE
	override_icon_state_held = TRUE

	desired_light_range = VIEW_RANGE*0.30
	desired_light_power = 0.50
	desired_light_color = "#FFD175"

	attack_delay = 5
	attack_delay_max = 10

	value = 5

	value = 30


/obj/item/weapon/melee/torch/flashlight
	name = "plastic flashlight"
	desc = "Can't live without it!"
	desc_extended = "Emits light in a certain radius and direction when activated."
	icon = 'icons/obj/item/weapons/melee/clubs/flashlight.dmi'

	max_charge = 6000
	charge = 6000

	damage_type = /damagetype/item/light
	damage_type_on = /damagetype/item/light

	override_icon_state = TRUE
	override_icon_state_held = TRUE

	desired_light_range = VIEW_RANGE*0.5
	desired_light_power = 0.5
	desired_light_color = "#FFF0C6"
	desired_light_angle = LIGHT_WIDE

	value = 20

/obj/item/weapon/melee/torch/flashlight/maglight
	name = "maglight"
	desc = "A robust flashlight."
	icon = 'icons/obj/item/weapons/melee/clubs/maglight.dmi'

	max_charge = 5000
	charge = 5000

	damage_type = /damagetype/item/medium
	damage_type_on = /damagetype/item/medium

	override_icon_state = TRUE
	override_icon_state_held = TRUE

	desired_light_range = VIEW_RANGE
	desired_light_power = 0.6
	desired_light_color = "#FFF0C6"
	desired_light_angle = LIGHT_NARROW

	value = 80

/obj/item/weapon/melee/torch/flashlight/meh_gen/type_1
	name = "FNM Type 1"
	desc = "Фонарь Нажимного Действия. Очень шумный для зарядки, но может моментально включаться."
	icon = 'icons/obj/item/weapons/melee/clubs/fnm.dmi'

	max_charge = 2000
	charge = 1020 //Дабы уж точно не забыли крутилку

	damage_type = /damagetype/item/medium
	damage_type_on = /damagetype/item/medium

	override_icon_state = TRUE
	override_icon_state_held = TRUE

	desired_light_range = VIEW_RANGE*0.5
	desired_light_power = 0.4
	desired_light_color = "#FFF0C6"
	desired_light_angle = LIGHT_NARROW

	value = 60

/obj/item/weapon/melee/torch/flashlight/meh_gen/type_2
	name = "FNM Type 2"
	desc = "Фонарь Нажимного Действия. Этот вроде шумит меньше, но пока зарядишь..."
	icon = 'icons/obj/item/weapons/melee/clubs/fnm.dmi'

	max_charge = 1000
	charge = 0
	damage_type = /damagetype/item/medium
	damage_type_on = /damagetype/item/medium

	override_icon_state = TRUE
	override_icon_state_held = TRUE

	desired_light_range = 1
	desired_light_power = 1
	desired_light_color = "#FFF0C6"
	desired_light_angle = LIGHT_NARROW

	value = 60

/obj/item/fuel/lant
	name = "Lantern fuel"
	desc = "Lantern can't live without it!"
	desc_extended = "Helps emit light in a certain radius and direction near light source"
	icon = 'icons/stalker/lohweb/drinks.dmi'
	icon_state = "goblet_artsilver1"

/obj/item/fuel/flash
	name = "Flashlight cell"
	desc = "Flashlight can't live without it!"
	desc_extended = "Helps emit light in a certain radius and direction near light source"
	icon = 'icons/items3.dmi'
	icon_state = "battery2"

/obj/item/fuel/meh_gen
	name = "Flashlight handler"
	desc = "Flashlight can't live without that generator!"
	desc_extended = "Helps emit light in a certain radius and direction near light source"
	icon = 'icons/stalker/lohweb/device.dmi'
	icon_state = "fixboy"

/obj/item/weapon/melee/torch/lantern/admin
	name = "Admin lantern"
	desc = "But what type of lantern? Eh, who cares."
	desc_extended = "Emits a lot of light."
	icon = 'icons/obj/item/weapons/melee/clubs/lantern.dmi'

	max_charge = INFINITY
	charge = INFINITY

	override_icon_state = TRUE
	override_icon_state_held = TRUE

	desired_light_range = VIEW_RANGE*1.5
	desired_light_power = 10
	desired_light_color = "#FFD175"

	attack_delay = 1
	attack_delay_max = 1