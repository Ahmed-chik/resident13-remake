/obj/item/weapon/melee/resident/powerfist
	name = "powerfist"
	desc = "A metal gauntlet with a energy-powered fist to throw back enemies. Altclick to clamp it around your hand, use it to change power settings and screwdriver to pop out the cell."
	icon = 'icons/obj/item/weapons/melee/resident/powerfist.dmi'
	damage_type = /damagetype/melee/club/power/

/obj/item/weapon/melee/resident/powerfist/click_on_object(var/mob/living/caller as mob,var/mob/living/M,location,control,params)

	INTERACT_CHECK

	var/x_mod = src.x
	var/y_mod = src.y

	var/max = max(abs(x_mod),abs(y_mod))

	if(!max)
		x_mod = pick(-1,1)
		y_mod = pick(-1,1)
	else
		x_mod *= 1/max
		y_mod *= 1/max

	if(is_living(M))
		M.throw_self(M,null,null,null,x_mod*10,y_mod*10)
		M.visible_message("<span class='danger'>[caller]'s powerfist shudders as they punch [M.name], flinging them away!</span>", \
			"<span class='userdanger'>[caller]'s punch flings you backwards!</span>")
//	playsound(loc, 'sound/weapons/energy_blast.ogg', 50, TRUE)
//	playsound(loc, 'sound/weapons/genhit2.ogg', 50, TRUE)

