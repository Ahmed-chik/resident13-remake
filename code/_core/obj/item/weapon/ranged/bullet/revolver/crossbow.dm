/obj/item/weapon/ranged/bullet/revolver/crossbow
	name = "\improper �������"
	desc = "������� �������."
	desc_extended = "������, ��������� ���������� ��� ��� �����, ������� �� ����� ����� ������."
	icon = 'icons/obj/item/weapons/ranged/revolver/crossbow.dmi'
	icon_state = "inventory"

	shoot_delay = 3

	automatic = FALSE

	bullet_count_max = 1

	insert_limit = 1

	view_punch = 12

	shoot_sounds = list('sound/weapons/combat_shotgun/shoot.ogg')

	slowdown_mul_held = HELD_SLOWDOWN_SHOTGUN_SMALL

	shoot_alert = ALERT_LEVEL_NONE

	size = SIZE_3
	weight = WEIGHT_3

	bullet_length_min = 40
	bullet_length_best = 45
	bullet_length_max = 46

	bullet_diameter_min = 5.5
	bullet_diameter_best = 5.56
	bullet_diameter_max = 5.6

	heat_per_shot = 0.05
	heat_max = 0.1

	value = 60

/obj/item/weapon/ranged/bullet/revolver/crossbow/get_base_spread()
	return 0.1

/obj/item/weapon/ranged/bullet/revolver/crossbow/get_static_spread() //Base spread
	return 0.01

/obj/item/weapon/ranged/bullet/revolver/crossbow/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.03 - (0.12 * L.get_skill_power(SKILL_RANGED)))