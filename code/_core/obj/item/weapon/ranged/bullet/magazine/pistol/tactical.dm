/obj/item/weapon/ranged/bullet/magazine/pistol/tactical
	name = ".45 1911 Pistol"
	desc = "Старый добрый М1911."
	desc_extended = "Самозарядный пистолет под патрон .45 ACP. Имеет боезапас в виде магазина на 8 выстрелов."
	value = 30

	icon = 'icons/obj/item/weapons/ranged/pistol/45.dmi'
	shoot_delay = 3
	shoot_sounds = list('sound/weapons/45/shoot.ogg')

	view_punch = 6

	automatic = FALSE

	size = SIZE_2
	weight = WEIGHT_2

	heat_per_shot = 0.03
	heat_max = 0.12

	bullet_length_min = 20
	bullet_length_best = 23
	bullet_length_max = 24

	bullet_diameter_min = 11
	bullet_diameter_best = 11.43
	bullet_diameter_max = 12

/obj/item/weapon/ranged/bullet/magazine/pistol/tactical/get_static_spread() //Base spread
	return 0.005

/obj/item/weapon/ranged/bullet/magazine/pistol/tactical/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.02 - (0.03 * L.get_skill_power(SKILL_RANGED)))

/obj/item/weapon/ranged/bullet/magazine/pistol/tactical/mod
	name = ".45 1911 Pistol MOD"
	desc = "Кольт. Джеймс Кольт"
	desc_extended = "Лучшее что в принципе могло случится в твоей жизни. Одно из самых практичных орудий убийства в наше время. От своего брата поменьше отличается лучшей точностью, надёжностью и скорострельностью. Классический автоматический «кольт», с честью прошедший все вооружённые конфликты XX века и уверенно вошедший в новое столетие."
	value = 50
	icon = 'icons/obj/item/weapons/ranged/pistol/45_2.dmi'
	shoot_delay = 3
	shoot_sounds = list('sound/weapons/45/shoot_mod.ogg')

	view_punch = 3

	size = SIZE_2
	weight = WEIGHT_3

	heat_per_shot = 0.02
	heat_max = 0.12

/obj/item/weapon/ranged/bullet/magazine/pistol/tactical/mod/get_static_spread() //Base spread
	return 0

/obj/item/weapon/ranged/bullet/magazine/pistol/tactical/mod/get_skill_spread(var/mob/living/L) //Base spread
	return max(0,0.02 - (0.02 * L.get_skill_power(SKILL_RANGED)))