/obj/item/weapon/ranged/bullet/magazine/rifle/standard
	name = "\improper Штурмовая винтовка B35"
	desc = "Ещё один пережиток прошлого."
	desc_extended = "Один из немногих образцов западного оружия, доживший до сегодняшних дней. Раритет, превосходящий ту же М40 практически по всем параметрам кроме, пожалуй, скорострельности."
	icon = 'icons/obj/item/weapons/ranged/rifle/556_nt.dmi'
	icon_state = "inventory"
	value = 10

	shoot_delay = 2

	automatic = TRUE

	shoot_sounds = list('sound/weapons/223/shoot.ogg')

	can_wield = TRUE

	view_punch = 10

	slowdown_mul_held = HELD_SLOWDOWN_RIFLE

	size = SIZE_5
	weight = WEIGHT_4

	heat_per_shot = 0.04
	heat_max = 0.08

	bullet_length_min = 40
	bullet_length_best = 45
	bullet_length_max = 46

	bullet_diameter_min = 5.5
	bullet_diameter_best = 5.56
	bullet_diameter_max = 5.6

	ai_heat_sensitivity = 0.5


/obj/item/weapon/ranged/bullet/magazine/rifle/standard/get_static_spread() //Base spread
	if(!wielded)
		return 0.01
	return 0

/obj/item/weapon/ranged/bullet/magazine/rifle/standard/get_skill_spread(var/mob/living/L) //Base spread
	if(!heat_current)
		return 0
	return max(0,0.02 - (0.04 * L.get_skill_power(SKILL_RANGED)))