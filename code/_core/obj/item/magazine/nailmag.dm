/obj/item/magazine/nailmag
	name = "\improper ������� ��� ������������� ���������"
	desc = "IT'S NOT A CLIP. IT'S A MAGAZINE."
	desc_extended = "Contains ammunition for a ranged weapon. Make sure you're trying to use the right caliber."
	icon = 'icons/obj/item/magazine/nail.dmi'
	icon_state = "nail"
	bullet_count_max = 10

	weapon_whitelist = list(
		/obj/item/weapon/ranged/bullet/magazine/pistol/nailgun = TRUE
	)

	bullet_length_min = 25
	bullet_length_best = 30
	bullet_length_max = 31

	bullet_diameter_min = 4
	bullet_diameter_best = 4.6
	bullet_diameter_max = 5

	ammo = /obj/item/bullet_cartridge/nail

	size = SIZE_2
	weight = WEIGHT_2

/obj/item/magazine/nailmag/update_icon()
	icon_state = "[initial(icon_state)]_[length(stored_bullets)]"
	return ..()