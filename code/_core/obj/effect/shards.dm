/*/obj/effect/steklo
	name = "glass shards"
	icon = 'floors.dmi'
	icon_state = "shard1"
	layer = LAYER_GROUND_SCENERY
	plane = -10

/obj/effect/steklo/New()
	icon_state = "shard[rand(1, 3)]"

/obj/effect/steklo/Crossed(var/atom/movable/O)

	. = ..()

	if(is_living(O))
		var/mob/living/L = O
		play('sound/items/drop/ring.ogg',get_turf(L))
		L.visible_message(span("danger","[O.name] наступает на стекло!"))
		create_alert(VIEW_RANGE,L,L,ALERT_LEVEL_NOISE)

	return .

/obj/effect/steklo/click_on_object(var/mob/caller as mob,var/atom/object,location,control,params)

	var/obj/effect/steklo/G = object
	caller.visible_message(span("[caller.name] собирает стекло с пола"))
	qdel(G)
	CREATE(/obj/item/material/shard,src.loc)*/

/obj/structure/interactive/steklo
	name = "glass shards"
	icon = 'floors.dmi'
	icon_state = "shard1"
	plane = -10

/obj/structure/interactive/steklo/New()
	icon_state = "shard[rand(1, 3)]"

/obj/structure/interactive/steklo/Crossed(var/atom/movable/O)

	. = ..()

	if(is_living(O))
		var/mob/living/L = O
		play('sound/items/drop/ring.ogg',get_turf(L))
		L.visible_message(span("danger","[O.name] наступает на стекло!"))
		create_alert(VIEW_RANGE,L,L,ALERT_LEVEL_NOISE)

	return .

/obj/structure/interactive/steklo/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller
	var/atom/defer_object = object.defer_click_on_object(location,control,params)

	if(!is_inventory(defer_object))
		P.to_chat(span("notice","С вещами в руках копаться в стекле не лучшая идея!"))
		return TRUE

	var/obj/hud/inventory/I = defer_object

	spawn()
		var/obj/item/new_item = new /obj/item/resident/crafting/shard(src)
		INITIALIZE(new_item)
		GENERATE(new_item)
		new_item.update_sprite()
		I.add_object(new_item,TRUE)
		P.to_chat(span("notice","Ты убираешь с пола стекло."))
		caller.visible_message(span("notice","[caller.name] собирает стекло с пола"))
		qdel(src)

	return TRUE