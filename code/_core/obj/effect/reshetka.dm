obj/effect/reshetka
	name = "lattice"
	mouse_opacity = 1
	layer = LAYER_GROUND_SCENERY
	icon = 'icons/stalker/lestnisa.dmi'
	icon_state = "setka2"

obj/effect/rails
	name = "rails"
	mouse_opacity = 1
	layer = LAYER_GROUND_SCENERY
	icon = 'icons/rails.dmi'
	icon_state = "rail"
	plane = -11

obj/effect/rails_moving
	name = "rails"
	mouse_opacity = 1
	layer = LAYER_GROUND_SCENERY
	icon = 'icons/rails.dmi'
	icon_state = "moving"
	plane = -11