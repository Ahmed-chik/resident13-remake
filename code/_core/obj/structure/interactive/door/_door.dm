#define DOOR_STATE_OPENED "opened"
#define DOOR_STATE_OPENING_01 "opening_01"
#define DOOR_STATE_OPENING_02 "opening_02"
#define DOOR_STATE_CLOSED "closed"
#define DOOR_STATE_CLOSING_01 "closing_01"
#define DOOR_STATE_CLOSING_02 "closing_02"
#define DOOR_STATE_LOCKED "locked" //Only used for singleplayer airlocks
#define DOOR_STATE_START_OPENING "start_opening" //Only used for airlocks
#define DOOR_STATE_DENY "denay" //Only used for airlocks
#define BASE 1
#define HYDRO 2
#define BAR 3
#define CIVIL 4
#define WAREHOUSE 5
#define OUTLANDS_1 11
#define OUTLANDS_2 12
#define OUTLANDS_3 13
#define OUTLANDS_4 14
#define OUTLANDS_5 15
#define OUTLANDS_6 16
#define OUTLANDS_7 17
#define OUTLANDS_8 18

/obj/structure/interactive/door
	name = "door"
	desc = "What's on the other side?"
	collision_flags = FLAG_COLLISION_WALL
	collision_bullet_flags = FLAG_COLLISION_BULLET_INORGANIC

	opacity = 1
	icon = 'icons/obj/structure/airlock/doors.dmi'
	icon_state = "woodrustic"

	var/door_state = DOOR_STATE_CLOSED

	var/open_time = 8
	var/close_time = 8

	var/locked = FALSE
	var/allow_manual_open = TRUE
	var/allow_manual_close = TRUE
	var/req_access = 0
	var/identification = 0
	var/power_need = 0
	var/energy = 0

	var/open_sound = null
	var/close_sound = null
	var/deny_sound = null

	var/spawn_signaller = FALSE
	var/radio_frequency = RADIO_FREQ_DOOR
	var/radio_signal = 20

	layer = LAYER_OBJ_DOOR_CLOSED

	plane = PLANE_WALL

/obj/structure/interactive/door/New(var/desired_loc)

	if(spawn_signaller)
		var/obj/item/device/signaller/S = new(src)
		S.name = "[name] [S.name]"
		S.frequency_current = radio_frequency
		S.signal_current = radio_signal
		INITIALIZE(S)
		GENERATE(S)
		door_state = DOOR_STATE_CLOSED
		locked = TRUE

	return ..()

/obj/structure/interactive/door/PostInitialize()
	. = ..()
	update_sprite()
	return .

obj/structure/interactive/door/update_icon()
	..()
	switch(door_state)
		if(DOOR_STATE_OPENING_01)
			icon_state = "[initial(icon_state)]opening"
			desc = "The door is opening."
			update_collisions(FLAG_COLLISION_WALL,FLAG_COLLISION_BULLET_INORGANIC)
			layer = LAYER_OBJ_DOOR_CLOSED
			set_opacity(0)

		if(DOOR_STATE_CLOSING_01)
			icon_state = "[initial(icon_state)]closing"
			desc = "The door is closing."
			update_collisions(FLAG_COLLISION_NONE,FLAG_COLLISION_BULLET_NONE)
			layer = LAYER_OBJ_DOOR_CLOSED
			set_opacity(0)

		if(DOOR_STATE_OPENED)
			icon_state = "[initial(icon_state)]open"
			desc = "The door is open."
			update_collisions(FLAG_COLLISION_NONE,FLAG_COLLISION_BULLET_NONE)
			layer = LAYER_OBJ_DOOR_OPEN
			set_opacity(0)

		if(DOOR_STATE_CLOSED)
			icon_state = initial(icon_state)
			desc = "The door is closed."
			update_collisions(FLAG_COLLISION_WALL,FLAG_COLLISION_BULLET_INORGANIC)
			layer = LAYER_OBJ_DOOR_CLOSED
			set_opacity(initial(opacity))

obj/structure/interactive/door/proc/toggle(var/atom/caller,var/lock = FALSE,var/force = FALSE)
	if(door_state == DOOR_STATE_OPENED)
		close(caller)
		return TRUE
	else if(door_state == DOOR_STATE_CLOSED)
		open(caller)
		return TRUE
	return FALSE

obj/structure/interactive/door/proc/open(var/atom/caller,var/lock = FALSE,var/force = FALSE)
	if(open_sound)
		play(open_sound,src)
		if(caller) create_alert(VIEW_RANGE,src,caller,ALERT_LEVEL_NOISE)
	door_state = DOOR_STATE_OPENING_01
	update_sprite()
	spawn(open_time)
		door_state = DOOR_STATE_OPENED
		update_sprite()


obj/structure/interactive/door/proc/close(var/atom/caller,var/lock = FALSE,var/force = FALSE)
	if(close_sound)
		play(close_sound,src)
		if(caller) create_alert(VIEW_RANGE,src,caller,ALERT_LEVEL_NOISE)
	door_state = DOOR_STATE_CLOSING_01
	update_sprite()
	spawn(close_time)
		door_state = DOOR_STATE_CLOSED
		update_sprite()

/obj/structure/interactive/door/proc/unlock(var/atom/caller,var/force = FALSE)
	locked = FALSE
	update_sprite()
	return TRUE

/obj/structure/interactive/door/proc/lock(var/atom/caller,var/force = FALSE)
	locked = TRUE
	update_sprite()
	return TRUE

/obj/structure/interactive/door/clicked_on_by_object(var/mob/caller,object,location,control,params)

	INTERACT_CHECK

	var/atom/A = check_interactables(caller,object,location,control,params)
	if(A && A.clicked_on_by_object(caller,object,location,control,params))
		return TRUE

	if(istype(object, /obj/item/key))// /obj/item/key/K // && door_state == DOOR_STATE_CLOSED
		var/obj/item/key/K = object
		var/L[] = K.access
/*		if(!req_access)
			allowed = 1 */
		if(istype(K, /obj/item/key/fuel_rod))
			if(power_need == 1)
				caller.to_chat(span("notice","Can`t insert that right into the door"))
			else if(power_need == 0)
				caller.to_chat(span("notice","Inserting something like that in door's lock? What with that world?"))
			return TRUE
		if(!islist(req_access))
			caller.to_chat(span("notice","Door is without lock"))
			return TRUE
		if(!islist(L))
			caller.to_chat(span("notice","Broken key"))
			return TRUE
		var/i
		for(i in req_access)
			if(!(i in L))
				caller.to_chat(span("notice","Wrong key"))
				return TRUE//Если нет доступа
			if(i in L)
				if(locked == FALSE)
					lock(caller)
					caller.to_chat(span("notice","Locked"))
					return TRUE
				else if(locked == TRUE)
					unlock(caller)
					caller.to_chat(span("notice","Unlocked"))
					return TRUE
	/*	else if(locked == FALSE)
		if((power_need == 1 && energy == 1) || power_need == 0) //Если нужна энергия и она есть или если энергия НЕ нужна
			if(door_state == DOOR_STATE_OPENED && allow_manual_close)
				if(power_need == 1)
					caller.to_chat("You close a door with slighty touch")
				close(caller)
			else if(door_state == DOOR_STATE_CLOSED && allow_manual_open)
				if(power_need == 1)
					caller.to_chat("You open a door with slighty touch")
				open(caller)*/
	else if(((power_need == 1 && energy == 1) || power_need == 0) && locked == FALSE) //Если нужна энергия и она есть ИЛИ если энергия не нужна !И! дверь не закрыта
		if(door_state == DOOR_STATE_OPENED && allow_manual_close)
			if(power_need == 1)
				caller.to_chat(span("notice","You close a door with slighty touch"))
			close(caller)
		else if(door_state == DOOR_STATE_CLOSED && allow_manual_open)
			if(power_need == 1)
				caller.to_chat(span("notice","You open a door with slighty touch"))
			open(caller)
		return
	if(locked == TRUE && (power_need == 1 && energy == 1))
		caller.to_chat(span("notice","Door is locked and unpowered"))
	else if(power_need == 1 && energy == 0)
		caller.to_chat(span("notice","Door is unpowered"))
	else if(locked == TRUE)
		caller.to_chat(span("notice","Door is locked"))
	else if(power_need != 0 || locked != FALSE)
		caller.to_chat(span("notice","Broke something"))
	return TRUE

obj/structure/interactive/door/Cross(var/atom/movable/A,var/atom/NewLoc,var/atom/OldLoc)

	. = ..()

	if(!. && is_living(A) && door_state == DOOR_STATE_CLOSED && locked == FALSE && power_need == 0)
		var/mob/living/L = A
		if(!L.dead)
			open(A)

	return .

obj/structure/interactive/door/wood
	name = "wooden door"

obj/structure/interactive/door/metal
	name = "iron door"
	icon_state = "silver"
	color = "#888888"

obj/structure/interactive/door/resident/metal
	name = "metal door"
	icon_state = "metal2"
	icon = 'icons/doors.dmi'
	opacity = 0
	bullet_block_chance = 65

obj/structure/interactive/door/resident/metal/base
	req_access = list(BASE)
	locked = TRUE

obj/structure/interactive/door/resident/metal/hydro
	req_access = list(HYDRO)

obj/structure/interactive/door/resident/metal/bar
	req_access = list(BAR)

obj/structure/interactive/door/resident/metal/warehouse
	req_access = list(WAREHOUSE)

obj/structure/interactive/door/resident/metal/civil
	req_access = list(CIVIL)

obj/structure/interactive/door/resident/wood2/outlands_1
	req_access = list(OUTLANDS_1)
	locked = TRUE

obj/structure/interactive/door/resident/metal/outlands_2
	req_access = list(OUTLANDS_2)
	locked = TRUE

obj/structure/interactive/door/resident/metal/outlands_3
	req_access = list(OUTLANDS_3)

obj/structure/interactive/door/resident/metal/outlands_4
	req_access = list(OUTLANDS_4)
	locked = TRUE

obj/structure/interactive/door/resident/metal/outlands_5
	req_access = list(OUTLANDS_5)
	locked = TRUE

obj/structure/interactive/door/resident/metal/outlands_6
	req_access = list(OUTLANDS_6)
	locked = TRUE

obj/structure/interactive/door/resident/metal/outlands_7
	req_access = list(OUTLANDS_7)
	locked = TRUE

obj/structure/interactive/door/resident/metal/outlands_8
	req_access = list(OUTLANDS_8)
	locked = TRUE

/obj/structure/interactive/door/resident/power/metal
	name = "metal door"
	icon_state = "metal2"
	icon = 'icons/doors.dmi'
	desc = "Common metal door, but in left corner of it is small mechanism...Like generator"
	power_need = 1
	opacity = 0
	bullet_block_chance = 90

obj/structure/interactive/door/resident/wood
	name = "wood door"
	icon_state = "wood"
	icon = 'icons/doors.dmi'
	opacity = 1
	bullet_block_chance = 50

obj/structure/interactive/door/resident/wood2
	name = "wood door"
	icon_state = "old"
	icon = 'icons/doors.dmi'
	opacity = 1
	bullet_block_chance = 30

obj/structure/interactive/door/resident/fancy
	name = "wood door"
	icon_state = "fancy"
	icon = 'icons/doors.dmi'
	opacity = 1
	bullet_block_chance = 30

obj/structure/interactive/door/resident/fancy2
	name = "wood door"
	icon_state = "fancy2"
	icon = 'icons/doors.dmi'
	opacity = 0
	bullet_block_chance = 10

/obj/structure/interactive/door_generator
	name = "LEDG"
	desc = "Small mechanism like...Like generator. On it's corpse: #Local Emergency Door Generator#"
	icon_state = "fixboy2"
	icon = 'icons/stalker/lohweb/device.dmi'
	collision_flags = FLAG_COLLISION_NONE
	density = 1
	plane = -5
	var/powered = 0
	var/power_dist = 4

/obj/structure/interactive/door_generator/proc/power(object)
	var/obj/structure/interactive/door_generator/G = object
	for(var/obj/structure/interactive/door/resident/power/D in range(power_dist,G))
		D.energy = 1

/obj/structure/interactive/door_generator/clicked_on_by_object(var/mob/caller,object,location,control,params)

	INTERACT_CHECK

	var/atom/A = check_interactables(caller,object,location,control,params)
	if(A && A.clicked_on_by_object(caller,object,location,control,params))
		return TRUE
	var/obj/structure/interactive/door_generator/G
	if(istype(object, /obj/item/key/fuel_rod))
		var/obj/item/key/fuel_rod/R = object
		power(G)
		if(powered == 0)
			caller.to_chat(span("notice","Генератор заработал и тот начал громко гудеть!"))
			powered = 1
			qdel(R)
			create_alert(14, G, G)
		else if(powered == 1)
			caller.to_chat(span("notice","Can't be inserted there!"))
	else
		var/mob/living/advanced/H = caller
		if(istype(H))
			H.to_chat(span("notice","Rebooting generator"))
			spawn(20)
				if(G.powered == 1)
					power(G)
				create_alert(14, G, G)

/obj/machinery/button
	name = "button"
	desc = "A remote control switch."
	icon = 'icons/items.dmi'
	icon_state = "keys"
	var/skin = "doorctrl"
	var/identification = null
	anchored = 1

/obj/machinery/button/clicked_on_by_object(var/mob/caller,object,location,control,params)
	for(var/obj/structure/interactive/door/D in world)
		var/obj/machinery/button/B = src
		if(D.identification == B.identification)
			if(D.door_state == "opened" && D.allow_manual_close)
				D.close(D)
				caller.to_chat("Closing")
			else if(D.door_state == "closed" && D.allow_manual_open)
				D.open(D)
				caller.to_chat("Opening")

/obj/machinery/button/door
	name = "door button"
	desc = "A door remote control switch."
	identification = "one"

/obj/structure/interactive/gate
	name = "Gate"
	desc = "Everything what goes in doesn't come out"
	icon = 'icons/obj/effects/effects.dmi'
	icon_state = "curse"
	opacity = 1
	collision_flags = FLAG_COLLISION_WALL
	layer = LAYER_EFFECT
	plane = PLANE_MOB
	dir = NORTH //То, где вход должен быть

	var/sided = TRUE //Если да, то пройти можно лишь с одной стороны, если нет, то с любой
	var/obtainable = TRUE //Только лишь один человек может зайти?

	var/obtain = null //Тот, кому можно зайти(если пусто, то первый вошедший получает это право)

/obj/structure/interactive/gate/Initialize()
	. = ..()
	for(var/obj/structure/S in src.loc)
		if(!istype(S, /obj/structure/interactive/gate))
			S.collision_flags = FLAG_COLLISION_NONE

/obj/structure/interactive/gate/update_sprite()
	. = ..()

	var/desired_dense = 0x0

	if(dir == NORTH)
		pixel_x = 0
		pixel_y = 0
		desired_dense |= NORTH
	else if(dir == EAST)
		pixel_x = 0
		pixel_y = 0
		desired_dense |= EAST
	else if(dir == SOUTH)
		pixel_x = 0
		pixel_y = 0
		desired_dense |= SOUTH
	else if(dir == WEST)
		pixel_x = 0
		pixel_y = 0
		desired_dense |= WEST
	update_collisions(c_dir = desired_dense)

	return .

/obj/structure/interactive/gate/Cross/(var/mob/living/caller)

	var/G = src

	var/throw_velocity = 45
	var/throw_accuracy = 0.5
	var/move_dir = get_dir(G, caller)
	var/list/offset = direction_to_pixel_offset(move_dir)
	var/diff_x = offset[1] + RAND_PRECISE(-throw_accuracy,throw_accuracy)
	var/diff_y = offset[2] + RAND_PRECISE(-throw_accuracy,throw_accuracy)
	var/turf/R = locate(caller.x - offset[1]*2, caller.y - offset[2]*2, caller.z) //2-й тайл спереди
	//var/turf/S = locate(caller.x - offset[1], caller.y - offset[2], caller.z) //Тайл спереди

	if(!caller || !is_living(caller) || !caller.ckey_last) //Если нет наступающего, если наступающий не является мобом, если вызывающий\наступающий не имеет сикея(отсеем тут всяких)
		return FALSE

	if(sided == TRUE)
		if(dir != move_dir)
			return FALSE

	var/ti = 75 //Сколько будет идти весь процесс
	if(obtainable)
		if(obtain == null)
			obtain += caller.ckey_last
			animate(caller, pixel_x = initial(caller.pixel_x) - offset[1]*65, pixel_y = initial(caller.pixel_y) - offset[2]*65, time = ti)
			caller.add_status_effect(STAGGER,20,ti)
			caller.visible_message(span("danger","Violet thing starts to pulling [caller.name] in!"))
			spawn(ti)
				caller.Move(R, silent = TRUE)
				animate(caller, pixel_x = initial(caller.pixel_x), pixel_y = initial(caller.pixel_y), time = 1)
			return FALSE
		else if(obtain == caller.ckey_last)
			animate(caller, pixel_x = initial(caller.pixel_x) - offset[1]*65, pixel_y = initial(caller.pixel_y) - offset[2]*65, time = ti)
			caller.add_status_effect(STAGGER,20,ti)
			spawn(ti)
				caller.Move(R, silent = TRUE)
				animate(caller, pixel_x = initial(caller.pixel_x), pixel_y = initial(caller.pixel_y), time = 1)
			return FALSE
		else if(obtain != caller.ckey_last)
			caller.throw_self(G,null,null,null,diff_x*throw_velocity,diff_y*throw_velocity)
			caller.visible_message(span("danger","BACK, YOU SHOULD NOT BE THERE, INTRUDERS!"))
			caller.health.adjust_loss_smart(oxy = 20,update=TRUE)
			caller.health.adjust_stamina(-100)
			caller.add_status_effect(STAGGER,20,20)
			return FALSE
	else 
		animate(caller, pixel_x = initial(caller.pixel_x) - offset[1]*65, pixel_y = initial(caller.pixel_y) - offset[2]*65, time = ti)
		caller.add_status_effect(STAGGER,20,ti)
		caller.visible_message(span("danger","Violet thing starts to pulling [caller.name] in!"))
		spawn(ti)
			caller.Move(R, silent = TRUE)
			animate(caller, pixel_x = initial(caller.pixel_x), pixel_y = initial(caller.pixel_y), time = 1)
		return FALSE
	return FALSE

/obj/structure/interactive/gate/test_north
	name = "test north"
	obtain = 1 