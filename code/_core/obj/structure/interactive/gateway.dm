var/global/list/obj/structure/interactive/gateway/all_gateways = list()

/obj/structure/interactive/gateway
	name = "gateway"
	desc = "Brings you to places!"
	icon = 'icons/obj/structure/gateway.dmi'
	icon_state = "gateway_on"
	pixel_x = -32
	pixel_y = -32

	plane = PLANE_WALL_ATTACHMENTS

	var/enabled = TRUE

/*/obj/structure/interactive/gateway/update_icon()
	icon_state = initial(icon_state)
	if(enabled)
		icon_state = "[icon_state]_on"

	return ..()*/

/obj/structure/interactive/gateway/clicked_on_by_object(var/atom/caller,var/atom/object,location,control,params)

	INTERACT_CHECK

	if(!is_player(caller))
		return TRUE

	var/mob/living/advanced/player/P = caller
	var/atom/defer_object = object.defer_click_on_object(location,control,params)

	if(!is_inventory(defer_object))
		P.to_chat(span("notice","Портал не пропускает вещи!"))
		return TRUE

	var/obj/hud/inventory/I = defer_object

	spawn()
		var/obj/item/new_item = new /obj/item/key/base/outlands_8(src)
		INITIALIZE(new_item)
		GENERATE(new_item)
		new_item.update_sprite()
		I.add_object(new_item,TRUE)
		P.to_chat(span("danger","Вы видите яркую вспышку...Кто я?...Где я?...Ничего не помню..."))
		P.add_status_effect(STUN,10,10)
		caller.visible_message(span("danger","Портал испускает мощный всплеск энергии и гаснет!!"))
		icon_state = "gateway"
		enabled = FALSE

	return TRUE