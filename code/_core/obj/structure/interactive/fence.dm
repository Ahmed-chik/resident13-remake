obj/structure/interactive/fence
	name = "fence"
	icon = 'icons/obj/structure/fence.dmi'
	icon_state = "straight"

	collision_flags = FLAG_COLLISION_WALKING
	collision_bullet_flags = FLAG_COLLISION_NONE

	bullet_block_chance = 0

	pixel_y = 16

obj/structure/interactive/fence/Cross(var/atom/movable/O,var/atom/NewLoc,var/atom/OldLoc)

	if(is_living(O) && O.collision_flags & FLAG_COLLISION_WALKING)
		var/mob/living/L = O
		var/obj/structure/interactive/fence/T = locate() in OldLoc.contents
		if(T)
			return TRUE

		if(L.climb_counter >= 3)
			L.climb_counter = 0
			return TRUE

		L.climb_counter++

		return FALSE

	return ..()

obj/structure/interactive/fence/Crossed(var/atom/movable/O,var/atom/new_loc,var/atom/old_loc)
	if(old_loc && is_living(O) && O.collision_flags & FLAG_COLLISION_WALKING)
		var/mob/living/L = O
		var/obj/structure/interactive/fence/T = locate() in old_loc.contents
		if(!T)
			animate(L,pixel_z = initial(L.pixel_z) + 10,time = TICKS_TO_DECISECONDS(L.move_delay), easing = CIRCULAR_EASING | EASE_OUT)
			L.move_delay += DECISECONDS_TO_TICKS(10)

	return ..()

obj/structure/interactive/fence/Uncrossed(var/atom/movable/O,var/atom/new_loc,var/atom/old_loc)
	if(is_living(O) && O.collision_flags & FLAG_COLLISION_WALKING)
		var/mob/living/L = O
		var/obj/structure/interactive/fence/T = locate() in new_loc.contents
		if(!T)
			animate(L,pixel_z = initial(L.pixel_z),time = TICKS_TO_DECISECONDS(L.move_delay), easing = CIRCULAR_EASING | EASE_OUT)
			L.move_delay += DECISECONDS_TO_TICKS(5)

	return ..()

obj/structure/interactive/fence/end
	icon_state = "end"

obj/structure/interactive/fence/post
	icon_state = "post"

obj/structure/interactive/fence/corner
	icon_state = "corner"

obj/structure/interactive/fence_door
	icon = 'icons/obj/structure/fence.dmi'
	icon_state = "door_opened"
	collision_flags = FLAG_COLLISION_NONE
	collision_bullet_flags = FLAG_COLLISION_NONE

	name = "fence"

	bullet_block_chance = 0

	pixel_y = 16