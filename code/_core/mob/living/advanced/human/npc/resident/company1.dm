/mob/living/advanced/npc/unique/resident
	name = "civilian"
	queue_delete_on_death = FALSE
	health_base = 300 //Extra health so the escort mission isn't fucking hell.


/mob/living/advanced/npc/unique/resident/tommy/
	name = "Томми Фланаган"
	desc = "Он любит пушки."
	species = "human"
	gender = MALE
	sex = MALE
	dialogue_id = /dialogue/npc/resident/quest/tommy
	loyalty_tag = "NanoTrasen"
	dir = SOUTH

/mob/living/advanced/npc/unique/resident/tommy/Initialize()
	. = ..()
	change_organ_visual("skin", desired_color = "#7c4a2d")
	change_organ_visual("hair_head", desired_color = "#000000", desired_icon_state = "hair_bobcurl")
	change_organ_visual("hair_face", desired_color = "#000000", desired_icon_state = "facial_longbeard_s")
	update_all_blends()
	equip_loadout(/loadout/tommy)

	return .

/loadout/tommy
	spawning_items = list(
		/obj/item/clothing/underbottom/underwear/boxers,
		/obj/item/clothing/undertop/underwear/shirt,
		/obj/item/clothing/shirt/syndicate,
		/obj/item/clothing/pants/syndicate,
		/obj/item/clothing/feet/shoes/black_boots,
		/obj/item/clothing/feet/shoes/black_boots/left,
		/obj/item/clothing/back/storage/satchel,
		/obj/item/clothing/belt/storage/colored/black,
	)

/mob/living/advanced/npc/resident/sarah/
	name = "Сара Конрад"
	desc = "DOCTA'!!!"
	species = "human"
	gender = FEMALE
	sex = FEMALE
	dialogue_id = /dialogue/npc/resident/quest/sarah
	loyalty_tag = "NanoTrasen"
	dir = SOUTH

/mob/living/advanced/npc/resident/sarah/Initialize()
	. = ..()
	change_organ_visual("skin", desired_color = "#E0BCAA")
	change_organ_visual("hair_head", desired_color = "#7c460e", desired_icon_state = "hair_emofringe_s")
	update_all_blends()
	equip_loadout(/loadout/sarah)

	return .

/loadout/sarah
	spawning_items = list(
		/obj/item/clothing/uniform/res/destro_scientist,
		/obj/item/clothing/overwear/coat/labcoat,
		/obj/item/clothing/feet/shoes/black_boots,
		/obj/item/clothing/feet/shoes/black_boots/left,
		/obj/item/clothing/hands/gloves/colored,
		/obj/item/clothing/glasses/prescription,
		/obj/item/clothing/hands/gloves/colored/left
	)


/loadout/sarah/pre_add(var/mob/living/advanced/A,var/obj/item/I)

/*	if(istype(I,/obj/item/weapon/ranged/))
		var/obj/item/weapon/ranged/R = I
		if(R.firing_pin)
			R.firing_pin = /obj/item/firing_pin/electronic/iff/nanotrasen*/

	if(istype(I,/obj/item/weapon/ranged/bullet/magazine))
		var/obj/item/weapon/ranged/bullet/magazine/M = I
		if(SSweapons.weapon_to_magazine[M.type])
			M.stored_magazine = pick(SSweapons.weapon_to_magazine[M.type])

	return ..()

/mob/living/advanced/npc/unique/resident/soldier/
	name = "Житель лагеря"
	desc = "Просто среднестатистический обитатель этих мест."
	species = "human"
	gender = MALE
	sex = MALE
	dialogue_id = /dialogue/npc/resident/quest/dweller1
	loyalty_tag = "NanoTrasen"
	dir = SOUTH

/mob/living/advanced/npc/unique/resident/soldier/Initialize()
	. = ..()
	change_organ_visual("skin", desired_color = "#E0BCAA")
	change_organ_visual("hair_head", desired_color = "#faffb8", desired_icon_state = "hair_business2")
	update_all_blends()
	equip_loadout(/loadout/dweller1)

	return .

/loadout/dweller1
	spawning_items = list(
		/obj/item/clothing/underbottom/underwear/boxers,
		/obj/item/clothing/undertop/underwear/shirt,
		/obj/item/clothing/uniform/res/civ3,
		/obj/item/clothing/belt/storage/medical/filled,
		/obj/item/clothing/feet/shoes/colored/blacker,
		/obj/item/clothing/feet/shoes/colored/blacker/left
	)

/mob/living/advanced/npc/resident/dmitriy/
	name = "Дмитрий Карпатенко"
	desc = "Умеет бить лица."
	species = "human"
	gender = MALE
	sex = MALE
	ai = /ai/advanced/nanotrasen
	dialogue_id = /dialogue/npc/resident/quest/dima
	loyalty_tag = "NanoTrasen"
	dir = SOUTH
	health_base = 300

/mob/living/advanced/npc/resident/dmitriy/Initialize()
	. = ..()
	change_organ_visual("skin", desired_color = "#E0BCAA")
	change_organ_visual("hair_head", desired_color = "#3f2828", desired_icon_state = "hair_gelled")
	change_organ_visual("hair_face", desired_color = "#3f2828", desired_icon_state = "facial_chinlessbeard_s")
	src.add_organ(/obj/item/organ/internal/implant/head/loyalty/nanotrasen)
	update_all_blends()
	equip_loadout(/loadout/dima)

	return .

/loadout/dima
	spawning_items = list(
		/obj/item/clothing/underbottom/underwear/boxers,
		/obj/item/clothing/undertop/underwear/shirt,
		/obj/item/clothing/uniform/res/destro_op,
		/obj/item/clothing/belt/storage/colored/black,
		/obj/item/clothing/head/res/tactical_helmet,
		/obj/item/clothing/overwear/armor/res/destrovest,
		/obj/item/clothing/feet/shoes/black_boots,
		/obj/item/clothing/feet/shoes/black_boots/left,
		/obj/item/clothing/back/storage/satchel,
		/obj/item/weapon/ranged/bullet/magazine/rifle/nt_carbine,
		/obj/item/storage/pouch/double/black,
		/obj/item/magazine/rifle_556_1,
		/obj/item/magazine/rifle_556_1,
		/obj/item/magazine/rifle_556_1,
		/obj/item/magazine/rifle_556_1,
		/obj/item/magazine/rifle_556_1,
		/obj/item/magazine/rifle_556_1,
		/obj/item/magazine/rifle_556_1,
		/obj/item/magazine/rifle_556_1,
		/obj/item/magazine/rifle_556_1
	)

/loadout/dima/pre_add(var/mob/living/advanced/A,var/obj/item/I)

/*	if(istype(I,/obj/item/weapon/ranged/))
		var/obj/item/weapon/ranged/R = I
		if(R.firing_pin)
			R.firing_pin = /obj/item/firing_pin/electronic/iff/nanotrasen*/

	if(istype(I,/obj/item/weapon/ranged/bullet/magazine))
		var/obj/item/weapon/ranged/bullet/magazine/M = I
		if(SSweapons.weapon_to_magazine[M.type])
			M.stored_magazine = pick(SSweapons.weapon_to_magazine[M.type])

	return ..()

/mob/living/advanced/npc/unique/resident/vlad/
	name = "Владимир Троцкий"
	desc = "Ему точно больше 100-а."
	species = "human"
	gender = MALE
	sex = MALE
	dialogue_id = /dialogue/npc/resident/quest/vlad
	loyalty_tag = "NanoTrasen"
	dir = SOUTH

/mob/living/advanced/npc/unique/resident/vlad/Initialize()
	. = ..()
	change_organ_visual("skin", desired_color = "#E0BCAA")
	change_organ_visual("eyes", desired_color = "#FFFFFF")
	change_organ_visual("hair_head", desired_color = "#FFFFFF", desired_icon_state = "none")
	change_organ_visual("hair_face", desired_color = "#aaaaaa", desired_icon_state = "facial_wise")
	update_all_blends()
	equip_loadout(/loadout/vlad)

	return .

/loadout/vlad
	spawning_items = list(
		/obj/item/clothing/underbottom/underwear/boxers,
		/obj/item/clothing/undertop/underwear/shirt,
		/obj/item/clothing/uniform/res/civ3,
		/obj/item/clothing/feet/shoes/black_boots,
		/obj/item/clothing/feet/shoes/black_boots/left,
		/obj/item/clothing/back/storage/satchel,
		/obj/item/clothing/overwear/coat/res/civilian
	)

/mob/living/advanced/npc/unique/resident/stranger/
	name = "Незнакомец"
	desc = "Выглядит...Странно."
	species = "human"
	gender = MALE
	sex = MALE
	dialogue_id = /dialogue/npc/resident/quest/stranger
	loyalty_tag = "NanoTrasen"
	dir = SOUTH

/mob/living/advanced/npc/unique/resident/stranger/Initialize()
	. = ..()
	change_organ_visual("skin", desired_color = "#E0BCAA")
	change_organ_visual("eyes", desired_color = "#f5f5f5")
	change_organ_visual("hair_head", desired_color = "#3f2828", desired_icon_state = "hair_gelled")
	change_organ_visual("hair_face", desired_color = "#FFFFFF", desired_icon_state = "none")
	update_all_blends()
	equip_loadout(/loadout/stranger)

	return .

/loadout/stranger
	spawning_items = list(
		/obj/item/clothing/underbottom/underwear/boxers,
		/obj/item/clothing/undertop/underwear/shirt,
		/obj/item/clothing/uniform/res/destro_op,
		/obj/item/clothing/feet/shoes/black_boots,
		/obj/item/clothing/feet/shoes/black_boots/left,
		/obj/item/clothing/back/storage/satchel,
		/obj/item/clothing/overwear/coat/res/donor,
		/obj/item/clothing/neck/cape/resident,
		/obj/item/clothing/head/res/tactical_helmet
	)

/mob/living/advanced/npc/unique/resident/vlad/test
	name = "Владимир Троцкий"
	desc = "Ему точно больше 100-а."
	species = "human"
	gender = MALE
	sex = MALE
	dialogue_id = /dialogue/npc/resident/quest/vlad_test
	loyalty_tag = "NanoTrasen"
	dir = SOUTH

/mob/living/advanced/npc/unique/resident/vlad/test/Initialize()
	. = ..()
	change_organ_visual("skin", desired_color = "#E0BCAA")
	change_organ_visual("eyes", desired_color = "#FFFFFF")
	change_organ_visual("hair_head", desired_color = "#FFFFFF", desired_icon_state = "none")
	change_organ_visual("hair_face", desired_color = "#aaaaaa", desired_icon_state = "facial_wise")
	update_all_blends()
	equip_loadout(/loadout/vlad)

	return .

/mob/living/advanced/npc/resident/alexander/
	name = "Александр Стуков"
	desc = "Мужчина с явным пиратским прошлым."
	species = "human"
	gender = MALE
	sex = MALE
	loyalty_tag = "NanoTrasen"
	dialogue_id = /dialogue/npc/resident/quest/stukov
	dir = SOUTH

/mob/living/advanced/npc/resident/alexander/Initialize()
	. = ..()
	change_organ_visual("skin", desired_color = "#E0BCAA")
	change_organ_visual("hair_head", desired_color = "#000000", desired_icon_state = "hair_business3")
	change_organ_visual("hair_face", desired_color = "#000000", desired_icon_state = "facial_fullbeard")
	update_all_blends()
	equip_loadout(/loadout/stuka)

	return .

/loadout/stuka
	spawning_items = list(
		/obj/item/clothing/head/hat/hos,
		/obj/item/clothing/uniform/res/destro_op,
		/obj/item/clothing/glasses/sun,
		/obj/item/clothing/overwear/armor/res/destrovest,
		/obj/item/clothing/overwear/coat/hos,
		/obj/item/clothing/hands/gloves/colored/combat,
		/obj/item/clothing/hands/gloves/colored/combat/left,
		/obj/item/clothing/feet/shoes/colored/black,
		/obj/item/clothing/feet/shoes/colored/black/left,
		/obj/item/clothing/belt/storage/colored/black,
		/obj/item/clothing/back/storage/satchel,
		/obj/item/weapon/ranged/bullet/magazine/pistol/tactical/mod,
		/obj/item/storage/pouch/double/black,
		/obj/item/magazine/pistol_45/heat,
		/obj/item/magazine/pistol_45/heat,
		/obj/item/magazine/pistol_45/heat,
		/obj/item/magazine/pistol_45/heat,
		/obj/item/magazine/pistol_45/heat,
		/obj/item/magazine/pistol_45/heat,
		/obj/item/magazine/pistol_45/heat,
		/obj/item/magazine/pistol_45/heat,
		/obj/item/magazine/pistol_45/heat
	)

/loadout/stuka/pre_add(var/mob/living/advanced/A,var/obj/item/I)

/*	if(istype(I,/obj/item/weapon/ranged/))
		var/obj/item/weapon/ranged/R = I
		if(R.firing_pin)
			R.firing_pin = /obj/item/firing_pin/electronic/iff/nanotrasen*/

	if(istype(I,/obj/item/weapon/ranged/bullet/magazine))
		var/obj/item/weapon/ranged/bullet/magazine/M = I
		if(SSweapons.weapon_to_magazine[M.type])
			M.stored_magazine = pick(SSweapons.weapon_to_magazine[M.type])

	return ..()
