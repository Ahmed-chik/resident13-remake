/mob/living/advanced/npc/bandits
	name = "Бандит"
	desc = "Член одного из преступных образований, захвативших город."
	enable_AI = TRUE
	ai = /ai/advanced/syndicate
	class = /class/syndicate_soldier
	dialogue_id = /dialogue/npc/soldier
	species = "human"

	var/loadout_to_use = /loadout/bandit

/mob/living/advanced/npc/bandits/Destroy()
	if(SShorde && src in SShorde.tracked_enemies)
		SShorde.on_killed_syndicate(src)
	return ..()

/mob/living/advanced/npc/bandits/Bump(var/atom/Obstacle)

	if(istype(src,Obstacle) || istype(Obstacle,src))
		return TRUE

	return ..()

/mob/living/advanced/npc/bandits/post_death()
	SShorde.on_killed_syndicate(src)
	return ..()

/mob/living/advanced/npc/bandits/New(loc,desired_client)
	setup_sex()
	return ..()

/mob/living/advanced/npc/bandits/proc/setup_sex()
	gender = pick(MALE,FEMALE)
	sex = gender //oh god oh fuck what have i done
	return TRUE

/mob/living/advanced/npc/bandits/proc/setup_appearance()
	change_organ_visual("skin", desired_color = pick("#E0BCAA","#BC9E8F","#967F73","#7A675E"))

	var/list/valid_male_hair = list("none","hair_a","hair_c","hair_d","hair_e","hair_f","hair_boss","hair_business2","hair_business","hair_afro2_s","hair_afro","hair_crewcut")
	var/list/valid_female_hair = list("hair_b","hair_ponytail2","hair_ponytail5","hair_vlong","hair_longfringe","hair_emo","hair_ponytail3","hair_emofringe_s")
	change_organ_visual("hair_head", desired_icon_state = sex == MALE ? pick(valid_male_hair) : pick(valid_female_hair), desired_color = pick("#111111","#404040","#54341F","#D8BB6A"))
	src.add_organ(/obj/item/organ/internal/implant/head/loyalty/syndicate)
	return TRUE

/mob/living/advanced/npc/bandits/Initialize()

	. = ..()

	setup_appearance()
	update_all_blends()
	equip_loadout(loadout_to_use)

	return .

/mob/living/advanced/npc/bandits/bolt
	loadout_to_use = /loadout/bandit/soldier

/mob/living/advanced/npc/bandits/otmichka
	loadout_to_use = /loadout/bandit/melee

/mob/living/advanced/npc/bandits/pahan
	loadout_to_use = /loadout/bandit/heavy