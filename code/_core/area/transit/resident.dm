/area/transit/dropship/train_main/bluespace
	id = "train1_shuttle_bluespace"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/train_main/landing
	id = "train1_shuttle_landing"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/train_main/station
	id = "train1_shuttle_station"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/train_sleep/bluespace
	id = "train2_shuttle_bluespace"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/train_sleep/landing
	id = "train2_shuttle_landing"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/train_sleep/station
	id = "train2_shuttle_station"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/elevator/bluespace
	id = "elev1_shuttle_bluespace"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/elevator/landing
	id = "elev1_shuttle_landing"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/elevator/station
	id = "elev1_shuttle_station"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/elevator2/bluespace
	id = "elev2_shuttle_bluespace"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/elevator2/landing
	id = "elev2_shuttle_landing"
	transit_turf = /turf/simulated/floor/plating

/area/transit/dropship/elevator2/station
	id = "elev2_shuttle_station"
	transit_turf = /turf/simulated/floor/plating